// DHT code Written by ladyada, public domain
// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include "DHT.h"
#define DHTPIN 2     // Digital pin connected to the DHT sensor

// Descomentar el tipo modelo de DHT
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Initialize DHT sensor.
DHT dht(DHTPIN, DHTTYPE);

//declaracion de variables sensor humedad suelo
const int sensorHumSuelo = A0;
int humedadSueloVal =0;
const int bomba = 9;

//Cambiar valores para calibrar
int humedadAire = 470;
int humedadAgua = 207;

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));
  dht.begin();
  pinMode(bomba, OUTPUT);
}

void loop() {
  // Wait a few seconds between measurements.
delay(2500);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humedad relativa: "));
  Serial.print(h);
  Serial.print(F("%  Temperatura: "));
  Serial.print(t);
  Serial.println(F("°C "));

   //bloque sensor humedad suelo
   int humedad = analogRead(sensorHumSuelo); 
   humedadSueloVal = map(humedad, humedadAire, humedadAgua, 0, 100);
   Serial.print("Humedad de suelo: ");
   Serial.print(humedadSueloVal);
   Serial.println("% "); 
   Serial.print("Valor humedad de suelo (para calibrar): ");
   Serial.println(humedad);

   //LOGICA
if (humedadSueloVal <20) {
   digitalWrite(bomba, LOW);
   Serial.println("Bomba PRENDIDA"); 
}  if ((humedadSueloVal>20) & (t>17)) {
  digitalWrite(bomba, HIGH);
  Serial.println("Bomba apagada"); 
} if ((humedadSueloVal<40) & (t<17)) {
   digitalWrite(bomba, LOW);
    Serial.println("Bomba prendida"); 
} if (humedadSueloVal>50) {
  digitalWrite(bomba, HIGH);
   Serial.println("Bomba apagada"); 
} 
  Serial.println("============================="); 

   delay(1000); 
}
