# Automatización de riego

Proyecto creado en el marco de la materia optativa: Tecnologias Libres, de la FCA-UNCUyo https://gitlab.com/nanocastro/teclibres-fca.
El objetivo fue crear un prototipo de un sistema de riego automatizado.

# Tutorial de construccion

[Publicacion en instructables, con descripcion paso a paso](https://www.instructables.com/Automatizaci%C3%B3n-De-Riego/)

# Autores y licencia
Ivan Garcia Escbar, Fernanda Flores

Licencia: CC BY-SA 


